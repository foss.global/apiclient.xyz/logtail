import { expect, tap } from '@pushrocks/tapbundle';
import * as plugins from './plugins';
const testQenv = new plugins.qenv.Qenv('./', './.nogit')


import * as logtail from '../ts/index';

let testLogtailAccount: logtail.LogTailAccount;

tap.test('should create a logtail account', async () => {
  testLogtailAccount = new logtail.LogTailAccount(testQenv.getEnvVarOnDemand('LOGTAIL_TOKEN'));
  expect(testLogtailAccount).to.be.instanceOf(logtail.LogTailAccount);
});

tap.test('should create a log message', async () => {
  let x = 0;  
  while (x < 3) {
    x++;
    await testLogtailAccount.log({
      level: 'silly',
      context: {
        environment: 'test',
        runtime: 'node',
        zone: 'gitzone',
        containerName: '@mojoio/logtail',
      },
      correlation: null,
      message: `a test from @mojoio/logtail ${x}`,
      timestamp: Date.now(),
      type: null,
      data: {'someData': 'a test from @mojoio/logtail'}
    })
  }
})

tap.start();
