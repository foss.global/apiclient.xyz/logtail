import * as plugins from './logtail.plugins';

export class LogTailAccount {
  private token: string;

  private timedAggregator = new plugins.lik.TimedAggregtor<plugins.smartlogInterfaces.ILogPackage<{[key: string]: any}>>({
    aggregationIntervalInMillis: 200,
    functionForAggregation: async (logPackagesArg) => {
      const requestBody = [];
      let lastTimestamp: number = 0;
      for (const logPackageArg of logPackagesArg) {
        if (logPackageArg.timestamp === lastTimestamp) {
          logPackageArg.timestamp++;
        }
        lastTimestamp = logPackageArg.timestamp;
        requestBody.push({
          dt: new Date(logPackageArg.timestamp).toISOString(),
          level: logPackageArg.level,
          message: `${logPackageArg.context?.containerName}: ${logPackageArg.message}`,
          originalMessage: logPackageArg.message,
          context: logPackageArg.context,
          correlation: logPackageArg.correlation,
          data: logPackageArg.data,
          type: logPackageArg.type
        });
      }
      const response = await plugins.smartrequest.request('https://in.logtail.com', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.token}`
        },
        requestBody,
        keepAlive: false,
      });
    }
  });

  constructor(logTailToken: string) {
    this.token = logTailToken;
  }

  async log(logPackageArg: plugins.smartlogInterfaces.ILogPackage<{[key: string]: any}>) {
    this.timedAggregator.add(logPackageArg);
  }

  public get smartlogDestination(): plugins.smartlogInterfaces.ILogDestination {
    return {
      handleLog: async (logPackage: plugins.smartlogInterfaces.ILogPackage) => {
        await this.log(logPackage);
      }
    }
  }
}